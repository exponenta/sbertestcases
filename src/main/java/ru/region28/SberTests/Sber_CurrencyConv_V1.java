package ru.region28.SberTests;

import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import ru.region28.SberTests.exceptions.TestsConfigurationException;
import ru.region28.SberTests.utils.TimeUtils;
import ru.region28.SberTests.webtestsbase.BasePage;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by eXponenta on 14.08.2016.
 */


class Sber_CurrencyConv_V1 extends BasePage {

    private final String URL = "http://www.sberbank.ru/ru/person/";

    private WebDriver driver;

    //что-то сломалось
    //@FindBy(css = "input[data-reactid=\".u.1.0.1.1.0\"]")
    @FindBy(css = "input.form_input_text#from")
    WebElement from_input;

    //@FindBy(css = "input[data-reactid=\".u.1.1.1.1.0\"]")
    @FindBy(css = "input.form_input_text#to")
    WebElement to_input;

	@FindBy(css="span[data-reactid=\".u.2.0\"]")
	WebElement left_result;

    @FindBy(css="span[data-reactid=\".u.2.4\"]")
    WebElement right_result;

    //@FindBy(css ="div[@data-reactid=\".u.1.0.1.0\"]")
    @FindBy(css ="div.form-group:nth-child(1) div.select2-container")
	WebElement FirstChooser;

    //@FindBy(xpath="//div[@data-reactid=\".u.1.1.1.0\"]")
    @FindBy(css="div.form-group:nth-child(2) div.select2-container")
    WebElement SecondChooser;

    Sber_CurrencyConv_V1(boolean openPageByUrl) {
        super(openPageByUrl,TestsConfig.getConfig().Delay_time());
    }

    @Override
    protected void openPage() {

        driver = getDriver();
        driver.get(URL);
        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
    }

    @Override
    protected void pageWasOpened() {

    }

    @Override
    public boolean isPageOpened() {
        return ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
        //return right_result != null;
    }

    //Конечно нужно было класс написать, но элемента всего два, и так сойдет
    //
    public void  FirstDropdownSelect(String val){
        DropChoose(FirstChooser, val);
    }

    public void  SecondDropdownSelect(String val){
        DropChoose(SecondChooser, val);
    }

    private void DropChoose(WebElement drop, String val) {


        //открываем окно выбора
        drop.click();
        //получаем список вариантов


        DelelteMask();

        boolean selected = false;
        //перебираем всех
        for (WebElement li: GetSelectedList()) {
            WebElement in = li.findElement(By.cssSelector("div"));
            if(in.getText().toUpperCase().contains(val.toUpperCase())){
                //
                li.click();
                selected = true;
                break;
            }
        }
        //на случай если не нашли вариант
        if(!selected) {
            drop.click();
            throw new TestsConfigurationException("Drop value \"" +val + "\" wasn't found!!" );
        }

        //дебага ради
        //TimeUtils.waitForSeconds(10);
    }

    public void DelelteMask(){

        //сносим id="select2-drop-mask" так как он не дает кликнуть на список
        //если еще не захайдили
        try {
            driver.findElement(By.id("select2-drop-mask"));
        } catch (NoSuchElementException e){
            return;
        }

        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("var m = document.getElementById(\"select2-drop-mask\");" +
                        "m.parentNode.removeChild(m);");
    }
    //Получение списка из dropdown
    public List<WebElement> GetSelectedList(){
        return driver.findElements(By.cssSelector("div.select2-drop.currency-converter-dropdown." +
                "select2-drop-active#select2-drop li.select2-result"));

    }
}
