package ru.region28.SberTests;

import ru.region28.SberTests.properties.PropertiesLoader;
import ru.region28.SberTests.properties.Property;
import ru.region28.SberTests.properties.PropertyFile;
import ru.region28.SberTests.webtestsbase.Browser;

/**
 * Created by Sidelnikov Mikhail on 18.09.14.
 * Class for base tests properties - urls for test, browser name and version
 */
@PropertyFile("config.properties")
public class TestsConfig {

    private static TestsConfig config;

    public static TestsConfig getConfig() {
        if (config == null) {
            config = new TestsConfig();
        }
        return config;
    }

    public TestsConfig() {
        PropertiesLoader.populate(this);
    }

    @Property("browser.name")
    private String browser = "chrome";

    @Property("chromedriver.path")
    public String chromedriver_path = "C:/chromedriver.exe";

    @Property("delay.time")
    private String delay_time = "10";

    public int Delay_time(){
        return Integer.parseInt(delay_time);
    }

    /**
     * getting browser object
     * @return browser object
     */
    public Browser getBrowser() {
        Browser browserForTests = Browser.getByName(browser);
        if (browserForTests != null) {
            return browserForTests;
        } else {
            throw new IllegalStateException("Browser name '" + browser + "' is not valid");
        }
    }
}
