package ru.region28.SberTests;
;
import junitparams.FileParameters;
import junitparams.JUnitParamsRunner;
import org.junit.*;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import ru.region28.SberTests.webtestsbase.WebDriverFactory;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.Title;

import java.util.concurrent.TimeUnit;

//@Ignore
@Stories("Тестирование на пустой ответ")
@Title("Тестирование на пустой ответ")
@Description(value = "Тестирование виджета конвертора валют на наличие пустого ответа" +
        "в различных сочетаниях денежных пар. \n")

@RunWith(JUnitParamsRunner.class)
public class TestOne extends Assert{


    private static Sber_CurrencyConv_V1 page;

    @Title("Загрузка страницы")
    @Step("Load page")
    @BeforeClass
    public static void RunDriver(){
        WebDriverFactory.startBrowser(true);
        WebDriverFactory.getDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        page = new Sber_CurrencyConv_V1(true);
    }

    @Step("Проверка виджета")
    @Description("Проверка элементов виджета на досягаемость")
    @Test
    public void FieldsNullTest()
    {

        assertNotNull("\"From\" field NULL!", page.from_input);
        assertNotNull("\"To\" field NULL!", page.to_input);
        assertNotNull("\"Left Result\" field NULL!", page.left_result);
        assertNotNull("\"Right Result\" field NULL!", page.right_result);
    }

    /*
    @Test
    public void ResultTest(String curr, int count){
        emptyCheck();
    }
*/
    @Step("Валюта c {0} конвертируется в {1} количеством {2} ")
    @Description("Тестирование на пустой результат ")
    @Test
    @FileParameters("src/test/resources/empty_result_data.csv")
    public void ResultEmptyTest(String from, String to, Integer count) {

        enterValue(count);
        selectConversionFirst(from);
        selectConversionSecond(to);

        String to_res = page.to_input.getAttribute("value");
        assertFalse("Result Value Empty!", to_res.isEmpty());
        log(to_res);
    }


    @Step("Вводим {0} ")
    private void enterValue(Integer val) {
        page.from_input.clear();
        page.from_input.sendKeys(val.toString());
        //page.from_input.sendKeys(Keys.ENTER);
    }


    @Step("Выбираем конвертацию {0} ")
    private void selectConversionFirst(String sel) {
        page.FirstDropdownSelect(sel);
    }


    @Step("в {0} ")
    private void selectConversionSecond(String sel) {
        page.SecondDropdownSelect(sel);
    }

    @Step("результат: {0}")
    private void log(String value) {
        //для логинга
    }

    /*
    @Step("Обновляем страницу")
    @After
    public void RefreshPage(){
        driver.navigate().refresh();
    }
    */

    @Step("Закрываем клиент")
    @AfterClass
    public static void CloseDriver(){
        WebDriverFactory.finishBrowser();
    }
}
