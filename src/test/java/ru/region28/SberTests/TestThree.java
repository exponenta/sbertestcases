package ru.region28.SberTests;

import junitparams.FileParameters;
import junitparams.JUnitParamsRunner;
import org.junit.*;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import ru.region28.SberTests.webtestsbase.WebDriverFactory;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.Title;

import java.util.concurrent.TimeUnit;


//@Ignore

@Stories("Тестирование на выбор валюты с клавиатуры")
@Title("Тестирование на выбор валюты с клавиатуры")
@Description(value = "Тестирование на возможность выбора валютной пары в селекторе с помощъю клавиатуры")

@RunWith(JUnitParamsRunner.class)
public class TestThree extends Assert{

    private static Sber_CurrencyConv_V1 page;

    @Title("Загрузка страницы")
    @Step("Load page")
    @BeforeClass
    public static void RunDriver(){

        WebDriverFactory.startBrowser(true);
        WebDriverFactory.getDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        page = new Sber_CurrencyConv_V1(true);
    }

    @Step("Проверка виджета")
    @Description("Проверка элементов виджета на досягаемость")
    @Test
    public void FieldsNullTest()
    {
        assertNotNull("\"From\" field NULL!", page.from_input);
        assertNotNull("\"To\" field NULL!", page.to_input);
        assertNotNull("\"First drop\" field NULL!", page.FirstChooser);
        assertNotNull("\"Second drop\" field NULL!", page.SecondChooser);
    }

    @Step("Отправляем кнопку {1} в селектор {0} с ожиданием {2} эелементов списка")
    @Test
    @FileParameters("src/test/resources/keyboard_selector_data.csv")
    public void KeyboardInputTest(Integer sell, String key, Integer res) {

        selectDrop(sell);
        sendKey(key);
        int ret = page.GetSelectedList().size();
        assertEquals("Получено неправильное количество ответов! ",(long)res,(long)ret);
        log("Ожидаемое количество ответов:" + ret);
    }

    @Step("Выбираем Dropdown")
    private void selectDrop(int id){
        try {
            WebDriverFactory.getDriver().findElement(By.id("select2-drop-mask")).click();
        } catch (Exception e){
        }
        //открываем и закрываем
        if(id==0){
            page.FirstChooser.click();
        }else{
            page.SecondChooser.click();
        }
        try {
            WebDriverFactory.getDriver().findElement(By.id("select2-drop-mask")).click();
        } catch (Exception e){
        }

    }

    @Step("Отправляем кнопку {0}")
    private void sendKey(String k){
        Actions acts = new Actions(WebDriverFactory.getDriver());
        acts.sendKeys(k);
        acts.build().perform();
    }

    @Step("результат: {0}")
    private void log(String value) {
        //для логинга
    }

    /*
    @Step("Обновляем страницу")
    @After
    public void RefreshPage(){
        driver.navigate().refresh();
    }
    */

    @Step("Закрываем клиент")
    @AfterClass
    public static void CloseDriver(){
        WebDriverFactory.finishBrowser();
    }
}
