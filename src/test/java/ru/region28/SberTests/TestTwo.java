package ru.region28.SberTests;

import junitparams.FileParameters;
import junitparams.JUnitParamsRunner;
import org.junit.*;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import ru.region28.SberTests.utils.TimeUtils;
import ru.region28.SberTests.webtestsbase.WebDriverFactory;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.Title;

import java.util.concurrent.TimeUnit;

//@Ignore

@Stories("Тестирование на фильтрацию неверных запросов")
@Title("Тестирование на фильтрацию неверных запросов")
@Description(value = "Тестирование виджета конвертора валют на фильтрацию плохих входных данных")

@RunWith(JUnitParamsRunner.class)
public class TestTwo extends Assert{

    private static Sber_CurrencyConv_V1 page;

    @Title("Загрузка страницы")
    @Step("Load page")
    @BeforeClass
    public static void RunDriver(){

        WebDriverFactory.startBrowser(true);
        WebDriverFactory.getDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        page = new Sber_CurrencyConv_V1(true);

    }

    @Step("Проверка виджета")
    @Description("Проверка элементов виджета на досягаемость")
    @Test
    public void FieldsNullTest()
    {

        assertNotNull("\"From\" field NULL!", page.from_input);
        assertNotNull("\"To\" field NULL!", page.to_input);
        assertNotNull("\"Left Result\" field NULL!", page.left_result);
        assertNotNull("\"Right Result\" field NULL!", page.right_result);
    }

    @Step("Вводим {0} ")
    @Test
    @FileParameters("src/test/resources/bad_input_data.csv")
    public void BadInputTest(String val) {

        page.from_input.clear();
        page.from_input.sendKeys(val);
        //TimeUtils.waitForSeconds(1);
        String from_res = page.from_input.getAttribute("value");
        assertFalse("Не отфильтровались!",val.contentEquals(from_res));
        log(from_res);
    }


    @Step("результат: {0}")
    private void log(String value) {
        //для логинга
    }

    /*
    @Step("Обновляем страницу")
    @After
    public void RefreshPage(){
        driver.navigate().refresh();
    }
    */

    @Step("Закрываем клиент")
    @AfterClass
    public static void CloseDriver(){
        WebDriverFactory.finishBrowser();
    }
}
